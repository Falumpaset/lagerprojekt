package projekt;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ReadPreference;
import com.mongodb.WriteConcern;

import backEnd.resources.AbstractEntity;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import dev.morphia.Datastore;
import dev.morphia.Morphia;

public class EmbeddedDataBase {

	private static final String DATABASE_NAME = "embedded";

	private static MongodExecutable mongodExe;
	private static MongodProcess mongod;
	private static MongoClient mongo;
	private static Datastore dataStore;

	public static final Datastore setUp() throws Exception {

		MongoClientOptions mongoOptions = MongoClientOptions.builder().socketTimeout(60000) // Wait 1m for a query to
				// finish,
				// https://jira.mongodb.org/browse/JAVA-1076
				.connectTimeout(15000) // Try the initial connection for 15s,
// http://blog.mongolab.com/2013/10/do-you-want-a-timeout/
				.maxConnectionIdleTime(600000) // Keep idle connections for 10m, so we discard failed connections
// quickly
				.readPreference(ReadPreference.primaryPreferred()) // Read from the primary, if not available use a
// secondary
				.writeConcern(WriteConcern.ACKNOWLEDGED).build();

		MongodStarter starter = MongodStarter.getDefaultInstance();
		String bindIp = "localhost";
		int port = 12345;
		IMongodConfig mongodConfig = new MongodConfigBuilder().version(Version.Main.PRODUCTION)
				.net(new Net(bindIp, port, Network.localhostIsIPv6())).build();
		mongodExe = starter.prepare(mongodConfig);
		mongod = mongodExe.start();
		mongo = new MongoClient(bindIp, mongoOptions);

		dataStore = new Morphia().mapPackage(AbstractEntity.class.getPackageName()).createDatastore(mongo,
				DATABASE_NAME);
		dataStore.ensureIndexes();
		dataStore.ensureCaps();

		var mapper = dataStore.getMapper().getOptions();
		mapper.setStoreNulls(true);
		mapper.setStoreNulls(true);

		return dataStore;
	}

	public static void shutdown() throws Exception {
		if (mongod != null) {
			mongod.stop();
			mongodExe.stop();
			dataStore.getDB().dropDatabase();
		}
	}
}
