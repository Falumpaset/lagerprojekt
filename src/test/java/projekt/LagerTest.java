package projekt;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import backEnd.database.MongoDriver;
import backEnd.resources.Order;
import backEnd.resources.ProductOrder;
import backEnd.resources.Resource;
import backEnd.resources.Warehouse;
import backEnd.resources.WarehouseOrder;

public class LagerTest {

	private static Warehouse testWarehouse;
	private static Warehouse testWarehouseExtra;
	private static Resource testResource;
	private static Resource testResourceExtra;
	private static Order testOrder;
	

	@BeforeClass
	public static void setUp() throws Exception {
		MongoDriver.setMongo(EmbeddedDataBase.setUp());

		testWarehouse = new Warehouse("testWarehouse", 30, 30);
		testWarehouseExtra = new Warehouse("testWarehouseExtra", 15, 15);
		testResource = new Resource("testResource");
		testResourceExtra = new Resource("testResourceExtra", 0.7);
			

		MongoDriver.save(testWarehouse);
		MongoDriver.save(testResource);
		MongoDriver.save(testResourceExtra);
	}

	@Before
	public void resetWarehouse() {
		testWarehouse.setStockForResource(testResource, 20);
		testWarehouse.setStockForResource(testResourceExtra, 20);

	}

	@Test
	public void dbTests() {

		assertTrue(MongoDriver.getById(Warehouse.class, testWarehouse.getId()).getStockMemory(testResource.getId()).toString().equals(testWarehouse.getStockMemory(testResource.getId()).toString()));
		assertTrue(MongoDriver.getById(Warehouse.class, testWarehouse.getId()).getId().equals(testWarehouse.getId()));
		assertTrue(MongoDriver.getById(Resource.class, testResource.getId()).equals(testResource));

		assertTrue(MongoDriver.getFeld(Warehouse.class, testWarehouse.getId()).getStockFor(testResource.getId()) == 20);

	}

	@Test
	public void wareHouseOrderTests() {

		var order = new WarehouseOrder();
		order.put(testResource.getId(), 20);
		order.put(testResourceExtra.getId(), 20);
		assertTrue(testWarehouse.unstockOrder(order));

		testWarehouse.soloStock(testResource.getId(), 20);
		var productOrder = new ProductOrder();
		productOrder.put(testResource.getId(), 0.02);
		assertTrue(testWarehouse.unstockOrder(productOrder));
		
		assertTrue(testWarehouse.getStockFor(testResource.getId()) == 19.98);

	}

	@Test
	public void wareHouseTrendTests() {
		assertFalse(testWarehouse.canDeliverStock(new Resource("djasd").getId(), 10));
		assertTrue(testWarehouse.canDeliverStock(testResource.getId(), 20));
		assertTrue(testWarehouse.getStockFor(testResource.getId()) == 20);
		testWarehouse.unStock(testResource.getId(), 10.0);
		assertTrue(testWarehouse.getTrendFor(testResource.getId()) == 1.0);
		assertTrue(testWarehouse.checkTrend(testResource.getId()));
		assertFalse(testWarehouse.checkTrend(testResourceExtra.getId()));
		testWarehouse.calculateTrend(new Resource("abs").getId());
		assertTrue(testWarehouse.getStockOnTheWay().containsKey(testResource.getId()));
		assertTrue(testWarehouse.getStockOnTheWay().get(testResource.getId()) == testWarehouse.getTimeTillRestock()
				* testWarehouse.getTrendFor(testResource.getId()) - testWarehouse.getStockFor(testResource.getId()));
	}
	
	@Test
	public void testCanDeliverOrder()
	{
		var order = new WarehouseOrder();
		order.put(testResource.getId(), 20);
		assertTrue(testWarehouse.canDeliverStock(testResource.getId(), order.get(testResource.getId()).doubleValue()));
		assertFalse(testWarehouse.canDeliverStock(testResource.getId(), 30));
		assertTrue(testWarehouse.canDeliverOrder(order));
		order.put(testResourceExtra.getId(), 30);
		assertFalse(testWarehouse.canDeliverOrder(order));
	}

	@AfterClass
	public static void after() throws Exception {
		EmbeddedDataBase.shutdown();
	}

}
