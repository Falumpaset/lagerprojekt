package projekt;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoException;

import backEnd.database.MongoDriver;
import backEnd.resources.User;
import backEnd.resources.Warehouse;
import backEnd.rest.AuthenticationFilter;
import backEnd.rest.AuthenticationHandler;
import backEnd.rest.JWTTokenHandler;
import backEnd.rest.LocalBarEndpoint;
import backEnd.rest.Roles;
import io.jsonwebtoken.security.SignatureException;

public class TokenTests {

	private static Warehouse warehouseInstance;
	private static User testUser;
	private static String testUserPasswordPlain = "test";

	@BeforeClass
	public static void setUp() {
		try {
			MongoDriver.setMongo(EmbeddedDataBase.setUp());
			warehouseInstance = new Warehouse("ABC", 30, 30);
			MongoDriver.save(warehouseInstance);
			LocalBarEndpoint.setLager(warehouseInstance);
			testUser = new User("test", testUserPasswordPlain, Roles.Admin);
			MongoDriver.save(testUser);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void saveUser() {

		assertTrue(MongoDriver.getByName(User.class, "test").getUsername().toString()
				.equals(testUser.getUsername().toString()));
		assertTrue(MongoDriver.getByName(User.class, "test").getPassword().toString()
				.equals(testUser.getPassword().toString()));
	}

	@Test(expected = DuplicateKeyException.class)
	public void saveDuplicateUser() {
		MongoDriver.save(new User("test", "test", Roles.Admin));
	}

	@Test
	public void authenticateUser() {
		AuthenticationHandler.authenticate(testUser.getUsername(), "test");
	}

	@Test(expected = IllegalArgumentException.class)
	public void authenticateFalseUser() {
		AuthenticationHandler.authenticate("wrongUser", "wrongPassword");
	}
	
	@Test
	public void issueAndCheckTokenCorrectUser() throws Exception
	{
		String token = AuthenticationHandler.authenticate(testUser.getUsername(),testUserPasswordPlain);

		assertTrue(JWTTokenHandler.readKey(token).equals(testUser.getRole()));
	}
	
	@Test (expected = SignatureException.class)
	public void issueAndCheckCorruptedToken()
	{
		String token = AuthenticationHandler.authenticate(testUser.getUsername(),testUserPasswordPlain);

		JWTTokenHandler.readKey(token+ "1");
	}

	@AfterClass
	public static void cleanUp() throws Exception{
		
			EmbeddedDataBase.shutdown();
		
	}

}
