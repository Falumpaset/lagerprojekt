package projekt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.bson.types.ObjectId;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Categories;

import com.mongodb.Mongo;
import com.sun.jersey.core.header.MediaTypes;

import backEnd.database.MongoDriver;
import backEnd.resources.Measure;
import backEnd.resources.Price;
import backEnd.resources.Product;
import backEnd.resources.Product.Category;
import backEnd.resources.ProductOrder;
import backEnd.resources.Resource;
import backEnd.resources.StockMemory;
import backEnd.resources.User;
import backEnd.resources.Warehouse;
import backEnd.resources.WarehouseOrder;
import backEnd.rest.AuthenticationHandler;
import backEnd.rest.JWTTokenHandler;
import backEnd.rest.LocalBarEndpoint;
import backEnd.rest.Roles;
import backEnd.rest.ServerConfig;

public class AuthenticationFilterTest extends JerseyTest {

	private static Warehouse testWarehouse;
	private static Warehouse testWarehouse2;
	private static Resource testResource;
	private static Resource testResource2;
	private static Product testProduct;
	private static String token;
	private static User testUser;

	@BeforeClass
	public static void start() throws Exception {

		testWarehouse = new Warehouse("test", 30, 30);
		testWarehouse2 = new Warehouse("test2", 30, 30);
		testResource = new Resource("testResource", 0.7);
		testResource2 = new Resource("testResource2", 0.5);

		LocalBarEndpoint.setLager(testWarehouse);
		MongoDriver.setMongo(EmbeddedDataBase.setUp());

		MongoDriver.save(testWarehouse);
		MongoDriver.save(testWarehouse2);
		MongoDriver.save(testResource);
		MongoDriver.save(testResource2);

		var ingredients = new HashMap<ObjectId, Measure>();
		ingredients.put(testResource.getId(), new Measure(0.2));
		testProduct = new Product("testProduct", ingredients, new Price(500), Category.Cocktail, 0.5);
		MongoDriver.save(testProduct);

		testUser = new User("testUsername", "testPassword", Roles.Admin);
		MongoDriver.save(testUser);
		token = AuthenticationHandler.authenticate(testUser.getUsername(), "testPassword");

		testWarehouse.soloStock(testResource.getId(), 10);

	}
	
	@Before
	public void resetStock()
	{
		testWarehouse.setStockForResource(testResource, 10);
	}

	@Override
	public Application configure() {
		enable(TestProperties.LOG_TRAFFIC);
		return new ServerConfig();
	}

	@Test
	public void givenWarehouseExists_whenSearching_thenResponseContainsWarehouseEntity() {
		String target = "db/Warehouse/" + testWarehouse.getId().toString() + "/get";

		Warehouse entity = target(target).request().header(HttpHeaders.AUTHORIZATION, token).get(Warehouse.class);
		assertTrue(entity.getName().equals(testWarehouse.getName()));
		assertTrue(entity.getStockFor(testResource.getId()) == 10);

	}
	
	@Test
	public void givenNotExistingResource_whenSucessful_ShouldReturnZero()
	{
		Response response = target("db/"+testResource2.getId()+"/getOverallStock").request().header(HttpHeaders.AUTHORIZATION, token)
				.get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertTrue(response.readEntity(Double.class)== 0.0);
	}

	@Test
	public void unstockOrderOverEndpoint_whenSucessful_StockChangedAccordingy() {
		WarehouseOrder stocking = new WarehouseOrder();
		stocking.put(testResource.getId(), 10);
		Response response = target("db/stock").request().header(HttpHeaders.AUTHORIZATION, token)
				.put(Entity.entity(stocking, MediaType.APPLICATION_JSON));
		assertEquals(Status.OK.getStatusCode(), response.getStatus());
		assertTrue(testWarehouse.getStockFor(testResource.getId()) == 20);
	}

	@Test
	public void givenExistingUser_whenSucessful_IssueToken() {

		var targetUrl = "db/" + testUser.getUsername() + "&testPassword/authenticate";
		String response = target(targetUrl).request(MediaType.APPLICATION_JSON).get(String.class);
		assertEquals(JWTTokenHandler.readKey(response), testUser.getRole());

	}

	@Test
	public void givenFalseUser_whenSucessful_Response401() {
		Response response = target("db/wrongUsername&wrongPassword/authenticate").request(MediaType.APPLICATION_JSON)
				.get();
		assertEquals(Status.UNAUTHORIZED.getStatusCode(), response.getStatus());

	}

	@Test
	public void givenWarehouseId_whenSucessful_thenResponseContainsWarehouseEntity() {
		Warehouse entity = target("db/Warehouse/" + testWarehouse.getId() + "/get").request()
				.header(HttpHeaders.AUTHORIZATION, token).get(Warehouse.class);
		assertTrue(entity.getName().equals(testWarehouse.getName()));
		assertTrue(entity.getStockFor(testResource.getId()) == 10);
	}

	@Test
	public void givenWrongWarehouseId_whenSearching_thenResponseShouldbe404() {
		Response response = target("db/Warehouse/" + testWarehouse.getId() + "2/get").request()
				.header(HttpHeaders.AUTHORIZATION, token).get();
		assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void givenRequest_whenSucessful_ReponseOverallStockofWarehouse() {
		HashMap<ObjectId, StockMemory> response = target("db/getOverallStock").request(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, token).get(new GenericType<HashMap<ObjectId, StockMemory>>() {
				});

		assertTrue(
				testWarehouse.getStockFor(testResource.getId()) == response.get(testResource.getId()).getActualStock());
	}

	@Test
	public void givenListOfObjectIds_whenSucessful_ReturnsListOfEnitys() {
		var objIdList = new ArrayList<String>();
		objIdList.add(testWarehouse.getId().toHexString());
		objIdList.add(testWarehouse2.getId().toHexString());

		Response response = target("db/Warehouse/getAllById").request().header(HttpHeaders.AUTHORIZATION, token)
				.post(Entity.json(objIdList));

		ArrayList<Warehouse> responseList = response.readEntity(new GenericType<ArrayList<Warehouse>>() {
		});

		assertEquals(testWarehouse.getId(), responseList.get(0).getId());
		assertEquals(testWarehouse2.getId(), responseList.get(1).getId());
		assertTrue(responseList.size() == 2);

	}

	@Test
	public void givenListContainingCorrectAndFalseObjectIds_whenSucessful_ResponseNotFound() {
		var objIdList = new ArrayList<String>();
		objIdList.add(testWarehouse.getId().toHexString());
		objIdList.add(testWarehouse2.getId().toHexString() + "2");

		Response response = target("db/Warehouse/getAllById").request().header(HttpHeaders.AUTHORIZATION, token)
				.post(Entity.json(objIdList));

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
	}

	@Test
	public void givenCategory_whenSucessful_ReturnsListOfEntitys() {
		ArrayList<Warehouse> response = target("db/Warehouse/getAll").request().header(HttpHeaders.AUTHORIZATION, token)
				.get(new GenericType<ArrayList<Warehouse>>() {
				});
		assertTrue(response.size() == 2);
		assertTrue(response.get(0).getId().equals(testWarehouse.getId()));
		assertTrue(response.get(1).getId().equals(testWarehouse2.getId()));
		assertTrue(response.get(0).toString().equals(testWarehouse.toString()));

	}

	@Test
	public void givenWrongCategory_whenSucessful_ReturnsNotFound() {
		Response response = target("db/WrongArgument/getAll").request().header(HttpHeaders.AUTHORIZATION, token).get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());

	}

	@Test
	public void givenProductOrder_whenSucessful_StockCHangesAccordingly() {

		var productOrder = new ProductOrder();
		productOrder.put(testResource.getId(), 2);
		Response response = target("db/unstockBar").request().header(HttpHeaders.AUTHORIZATION, token)
				.put(Entity.json(productOrder));
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertTrue(testWarehouse.getStockFor(testResource.getId()) == 10 - 2);
	}

	@Test
	public void givenProductOrderContainingFalseId_whenSUcessful_ReturnsBadRequest() {
		var productOrder = new ProductOrder();
		productOrder.put(testResource2.getId(), 2);
		Response response = target("db/unstockBar").request().header(HttpHeaders.AUTHORIZATION, token)
				.put(Entity.json(productOrder));

		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void givenWarehouseOrder_whenSucessful_StockShouldDecreaseAccordingly() {
		var warehouseOrder = new WarehouseOrder();
		warehouseOrder.put(testResource.getId(), 10);
		Response response = target("db/unstockWH").request().header(HttpHeaders.AUTHORIZATION, token)
				.put(Entity.json(warehouseOrder));
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertTrue(testWarehouse.getStockFor(testResource.getId()) == 10
				- warehouseOrder.get(testResource.getId()).intValue());
	}
	
	@Test
	public void givenFalseWarehouseOrder_whenSucessful_ResponseBadRequest() {
		var warehouseOrder = new WarehouseOrder();
		warehouseOrder.put(testResource.getId(), 12);
		Response response = target("db/unstockWH").request().header(HttpHeaders.AUTHORIZATION, token)
				.put(Entity.json(warehouseOrder));
		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
		
	}
	
	

	@AfterClass
	public static void after() throws Exception {
		EmbeddedDataBase.shutdown();
	}
}
