package backEnd;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import backEnd.database.MongoDriver;
import backEnd.rest.ServerConfig;

public class LagermanagementApplication

{
	final static MongoDriver driver = new MongoDriver();

	private final static URI BASE_URI = URI.create("http://localhost:8080/");

	public static HttpServer startServer() {
		final ResourceConfig rc = new ServerConfig();

		return GrizzlyHttpServerFactory.createHttpServer(BASE_URI, rc);
	}

	public static void main(String[] args) throws IOException {

		try {		
		
		final HttpServer server = startServer();
			System.out.println("Listening on " + BASE_URI);
			System.in.read();

			server.shutdownNow();

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

}
