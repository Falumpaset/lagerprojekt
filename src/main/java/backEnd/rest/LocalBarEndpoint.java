package backEnd.rest;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.bson.types.ObjectId;

import backEnd.database.MongoDriver;
import backEnd.resources.AbstractEntity;
import backEnd.resources.Product;
import backEnd.resources.ProductOrder;
import backEnd.resources.Resource;
import backEnd.resources.StockMemory;
import backEnd.resources.User;
import backEnd.resources.Warehouse;
import backEnd.resources.WarehouseOrder;

@Path("db")
public class LocalBarEndpoint {

	private static Warehouse warehouseInstance;
	
	/**
	 * This endpoint will be called to authenticate a new Client, if username and password are correct, we will return a new JWT, deny acess otherwise
	 * @param username
	 * @param password
	 * @return JWT or 401 
	 */

	@RolesAllowed("Anonymous")
	@GET
	@Path("{username}&{password}/authenticate")
	@Produces(APPLICATION_JSON)
	public Response authenticateUser(@PathParam("username") String username, @PathParam("password") String password) {
		try {

			return Response.ok(AuthenticationHandler.authenticate(username, password)).build();
		} catch (Exception e) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}
	
	/**
	 * Client wants to get Warehouse Entity with defined id 
	 * @param <E> Warehouse Entity
	 * @param id Warehouse Id 
	 * @return Warehouse Entity
	 */

	@RolesAllowed({ "User", "Admin", "WarehouseMgt" })
	@GET
	@Path("Warehouse/{id}/get")
	@Produces(APPLICATION_JSON)
	public <E extends AbstractEntity> Response getEntity(@PathParam("id") ObjectId id) {

		var entry = MongoDriver.getById(Warehouse.class, id);

		return Response.ok(entry).build();
	}

	/**
	 * Client wants to check Overall Stock of this WArehouse instance
	 * @return The overall Stock as Object
	 */
	@RolesAllowed({ "Admin", "WarehouseMgt" })
	@GET
	@Path("/getOverallStock")
	@Produces(APPLICATION_JSON)
	public Response getOverallStock() {
		var entry = warehouseInstance.getStock();

		return entry != null ? Response.ok(entry).build() : Response.status(Response.Status.NOT_FOUND).build();

	}
	
	/**
	 * Client want to check OverallStock of Resource with Id
	 * @param resource Warehouse Id
	 * @return Stock 
	 */

	@RolesAllowed({ "Admin", "WarehouseMgt" })
	@GET
	@Path("{id}/getOverallStock")
	@Produces(APPLICATION_JSON)
	public Response getResourceStock(@PathParam("id") ObjectId resource) {

		var entry = warehouseInstance.getStockFor(resource);
		return Response.ok(entry).build();
	}
	
	/**
	 * Client wants to get all Entities of given Class by Id
	 * @param <E> 
	 * @param clazz Class 
	 * @param ids List of Objects the Client wants to get
	 * @return List of Objets of requested Class
	 */

	@RolesAllowed({ "User", "Admin", "WarehouseMgt" })
	@POST
	@Path("{class}/getAllById")
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	public <E extends AbstractEntity> Response getAllById(@PathParam("class") Entitys clazz, List<String> ids) {
		ArrayList<ObjectId> idList = new ArrayList<ObjectId>();
		ids.forEach(x -> idList.add(new ObjectId(x)));

		var entry = MongoDriver.getByIdList(getClass(clazz), idList);
		return Response.ok(entry).build();
	}

	
	/**
	 * Client wants to get all Entities of given Category
	 * @param <E> 
	 * @param category requested Category
	 * @return All Entities as List
	 */
	@RolesAllowed({ "User", "Admin", "WarehouseMgt" })
	@GET
	@Path("{category}/getAll")
	@Produces(APPLICATION_JSON)
	public <E extends AbstractEntity> Response getAllEntitys(@PathParam("category") Entitys category) {

		List<? extends AbstractEntity> response = MongoDriver.getAll(getClass(category));

		return Response.ok(response).build();

	}
	
	/**
	 * Unstocks given Productorder from warehouse 
	 * @param order List of Producs
	 * @return Sucess / no Sucess
	 */

	@RolesAllowed({ "User", "Admin", "WarehouseMgt" })
	@PUT
	@Consumes(APPLICATION_JSON)
	@Path("/unstockBar")
	public Response unstockBar(ProductOrder order) {

		return warehouseInstance.unstockOrder(order) ? Response.status(Response.Status.OK).build()
				: Response.status(Response.Status.BAD_REQUEST).build();

	}
	
	/**
	 * Warehouse Control calls this Method to unstock a big order delivered to another Warehouse
	 * @param order Order to unstock
	 * @return Sucess / no Sucess
	 */

	@RolesAllowed({ "Admin", "WarehouseMgt", "User" })
	@PUT
	@Consumes(APPLICATION_JSON)
	@Path("/unstockWH")
	public Response unstockWH(WarehouseOrder order) {

		return warehouseInstance.unstockOrder(order) ? Response.status(Response.Status.OK).build()
				: Response.status(Response.Status.BAD_REQUEST).build();

	}
	
	
	/**
	 * Warehouse Control will call this Method to check in Stock into the Warehouse
	 * @param order Stock to check in	
	 * @return Sucess/ no sucess
	 */

	@RolesAllowed({ "Admin", "WarehouseMgt", "User" })
	@PUT
	@Consumes(APPLICATION_JSON)
	@Path("/stock")
	public Response stockBar(WarehouseOrder order) {

		warehouseInstance.multiStock(order);
		return Response.status(Response.Status.OK).build();
	}

	private Class<? extends AbstractEntity> getClass(Entitys clazz) throws IllegalArgumentException{

		switch (clazz) {
		case Warehouse:
			return Warehouse.class;

		case Resource:
			return Resource.class;

		case Product:
			return Product.class;

		default:
			throw new IllegalArgumentException();

		}

	}

	public static void setLager(Warehouse lager) {
		warehouseInstance = lager;
	}

}
