package backEnd.rest;

import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 * This FIlter verifys the access permissions for a user based on a JWT
 * 
 * @author marcjager
 *
 */
public class AuthenticationFilter implements ContainerRequestFilter {

	@Context
	private ResourceInfo resourceInfo;

	private static final String AUTHENTICATION_SCHEME = "AWS4-HMAC-SHA256";

	/**
	 * Filters incoming requests by checking if the token is legal
	 */
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		Method method = resourceInfo.getResourceMethod();

		Roles userRole;

		if (!method.isAnnotationPresent(RolesAllowed.class)) {
			throw new NotAuthorizedException("Not Allowed to access this Endpoint");
		}

		var resourceRoles = getRolesAllowed(method);

		String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

		try {
			userRole = authorizationHeader == null ? Roles.Anonymous : JWTTokenHandler.readKey(authorizationHeader);

			if (!resourceRoles.contains(userRole)) {
				abortWithUnauthorized(requestContext);
			}

		} catch (Exception e) {
			abortWithUnauthorized(requestContext);
		}

	}

	/**
	 * Checks if the Header fits the requested Format
	 * 
	 * @param authorizationHeader header extracted from http request
	 * @return
	 */

	private Set<Roles> getRolesAllowed(AnnotatedElement method) {

		return Arrays.stream(method.getAnnotation(RolesAllowed.class).value()).map(x -> Roles.valueOf(x))
				.collect(Collectors.toSet());

	}

	/**
	 * If the Header is invald this method will be called which will abort the
	 * Request with an Unauthorised response
	 * 
	 * @param requestContext the Http request
	 */

	private void abortWithUnauthorized(ContainerRequestContext requestContext) {

		requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
				.header(HttpHeaders.WWW_AUTHENTICATE, AUTHENTICATION_SCHEME + "Invaild Token").build());
	}

}
