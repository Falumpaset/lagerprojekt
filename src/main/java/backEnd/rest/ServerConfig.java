package backEnd.rest;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

import backEnd.resources.AbstractEntity;
import backEnd.resources.ObjectIdMapping;

public final class ServerConfig extends ResourceConfig
{
	public ServerConfig()
	{
		super();
		this.packages(LocalBarEndpoint.class.getPackageName())
		.packages(AbstractEntity.class.getPackageName())
		.register(new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME), Level.INFO,
				LoggingFeature.Verbosity.PAYLOAD_TEXT, 10000))
		.register(JacksonFeature.class).property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true)
		.register(AuthenticationFilter.class)
		.property(ServerProperties.WADL_FEATURE_DISABLE, true);
	}
}
