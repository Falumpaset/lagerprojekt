package backEnd.rest;

public enum Entitys {
	
		Warehouse, Resource, Product;
	
	public static Entitys fromString(final String s)
	{
		return Entitys.valueOf(s);
	}

}
