package backEnd.rest;

import backEnd.database.MongoDriver;
import backEnd.encryption.Encryption;
import backEnd.resources.User;

public class AuthenticationHandler {

	/**
	 * Athenticates a Request if a user submitts correct username and password
	 * combination and issues to token to authenticate further requests
	 * 
	 * @param username submitted username
	 * @param passwort submitted unencrypted password
	 * @return valid token
	 * @throws Exception if credentials dont match db entry
	 */
	public static String authenticate(String username, String password) throws IllegalArgumentException {

		var user = MongoDriver.getByName(User.class, username);

		if (!Encryption.decrypt(password, user.getPassword())) {
			throw new IllegalArgumentException();
		}

		
		String token = JWTTokenHandler.createToken(user.getId(), user.getRole());
		return token;

	}

}
