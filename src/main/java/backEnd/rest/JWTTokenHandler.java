package backEnd.rest;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import javax.crypto.SecretKey;

import org.bson.types.ObjectId;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

public class JWTTokenHandler {

	private static SecretKey SECRET_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS256);

	/**
	 * Creates a token Depending of declared role and objectId
	 * 
	 * @param id   the ObectId the user has to ensure User exists
	 * @param role the role User has
	 * @return JWS
	 * @throws IllegalArgumentException
	 */

	public static String createToken(ObjectId id, Roles role) throws IllegalArgumentException {

		Date now = new Date(System.currentTimeMillis());
		JwtBuilder builder = Jwts.builder().setId(id.toString()).setIssuedAt(now).claim("role", role.toString())
				.signWith(SECRET_KEY);

		return builder.compact();

	}

	/**
	 * Pareses Key and determines the Role if the key is valid
	 * 
	 * @param key the token to parse
	 * @return the role the uses has
	 * @throws UnsupportedEncodingException
	 */

	public static Roles readKey(String key) throws IllegalArgumentException {

		Jws<Claims> jws;

		jws = Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(key);

		var role = Roles.valueOf((String) jws.getBody().get("role"));

		return getRole(role);

	}

	/**
	 * Sets the role depending on Input String
	 * 
	 * @param role String representation
	 * @return Enum role
	 */

	private static Roles getRole(Roles role) {

		switch (role) {
		case Admin:
			return Roles.Admin;

		case User:
			return Roles.User;

		case WarehouseMgt:
			return Roles.WarehouseMgt;

		default:
			throw new IllegalArgumentException();

		}
	}

}
