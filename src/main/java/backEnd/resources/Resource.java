package backEnd.resources;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import dev.morphia.annotations.Entity;

@Entity(value = "rohstoffe", noClassnameStored = true)
public class Resource extends AbstractEntity {

	private String name;

	private double objectSize;

	/**
	 * @require name != null
	 */

	public Resource() {

	}

	public Resource(String name) {
		assert name != null;
		this.name = name;
		this.objectSize = 1;

	}

	@JsonCreator
	public Resource(@JsonProperty("name") String name, @JsonProperty("objectSize") double objectSze) {
		this.name = name;
		this.objectSize = objectSze;

	}

	public double getAmountRelativeToObjectSize(Measure amount) {

		return (this.objectSize / 100 * amount.getAmount() / 100);
	}

	public double getObjectSize() {
		return this.objectSize;
	}

	public void setObjectSize(double newAmount) {
		this.objectSize = newAmount;

	}

	public String getName() {
		return this.name;
	}

	public void setName(String firstName) {
		this.name = firstName;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof Resource) {
			Resource other = (Resource) obj;
			result = (name.equals(other.name));
		}
		return result;
	}

	@Override
	public String toString() {
		return "Rohstoff [id=" + id + ",name=" + name + ",einheitsgroeße=" + objectSize + "]";
	}

}
