package backEnd.resources;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import backEnd.database.MongoDriver;
import backEnd.encryption.Encryption;
import backEnd.rest.Roles;
import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Indexed;
import dev.morphia.annotations.Reference;

@Entity(value = "user", noClassnameStored = true)
public class User extends AbstractEntity {
	
	
	

	@Indexed(unique = true)
	private String name;

	@Reference(lazy = true, idOnly = true)
	private Password password;

	private Roles role;

	public User() {

	}

	public User(String name, String password, Roles role) {

		
		this.name = name;

		this.password = MongoDriver.save(new Password(Encryption.encrypt(password)));

		this.role = role;
	}

	@JsonCreator
	public User(@JsonProperty("name") String name, @JsonProperty("password") String password,
			@JsonProperty("role") String role) {
		
		

		this.name = name;

		this.password = new Password(password);

		this.role = Roles.valueOf(role);
	}
	


	public void setPassword(Password pw) {
		this.password = pw;
	}

	public String getPassword() {
		return this.password.getPassword();
	}

	public void setUsername(String name) {
		this.name = name;
	}

	public String getUsername() {
		return this.name;
	}

	public Roles getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = Roles.valueOf(role);
	}

	@Override
	public String toString() {
		return "User[name=" + name + "]";
	}

}
