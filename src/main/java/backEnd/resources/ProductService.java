package backEnd.resources;

import java.util.HashMap;

import org.bson.types.ObjectId;

import backEnd.resources.Product.Category;

public abstract interface ProductService
{
    public String getName();

    public Price getPrice();

    public void setName(String neuerName);

    public void setPrice(Price neuerPreis);

    public HashMap<ObjectId, Measure> getIngedients();

    public void setCategory(Category kategorie);

    public Category getCategory();

    public void addIngredient(Resource rohstoff, Measure menge);

    public void deleteIngredient(Resource rohstoff);

    public double getAmount();

    public String getFormattedString();

}
