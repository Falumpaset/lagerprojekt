package backEnd.resources;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import dev.morphia.annotations.Id;
import dev.morphia.annotations.Version;

public abstract class AbstractEntity {

	@Id
	protected ObjectId id;
	
	
	
	@Version
	private long version;
	
	public AbstractEntity ()
	{
		super();
	}
	
	@JsonSerialize(using=ObjectIdMapping.class) 
	public ObjectId getId()
	{
		return id;
	}
	@JsonSerialize(using=ObjectIdMapping.class) 
	public void setId(ObjectId id)
	{
		this.id = id;
	}
	
	
	@Override
	public abstract String toString();
	
}
