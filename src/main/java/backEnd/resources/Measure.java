package backEnd.resources;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import dev.morphia.annotations.Embedded;

@Embedded
public final class Measure
{
	
	
    private double amount;

    /**
     * Creates a new Measure Object, saves amount in Cl
     * @param amount in cl
     */
    
    public Measure()
    {
    	
    }
    
    
    public Measure(double amount, String unit)
    {
        switch (unit)
        {
        case "l":
            this.amount = amount * 100;
            break;

        case "ml":
            this.amount = amount / 10;
            break;

        case "cl":
            this.amount = amount;
        default:
            this.amount = 1;
        }

    }
    
    @JsonCreator
    public Measure(@JsonProperty("menge") double amount)
    {
    	this.amount = amount;

    }
    


    @Override
    public boolean equals(Object obj)

    {
        boolean result = false;
        if (obj instanceof Measure)
        {
            Measure other = (Measure) obj;
            result = (getAmount() == other.getAmount());
        }
        return result;
    }

    public double getAmount()
    {
        return amount;
    }
    
    public void setAmount(double menge)
    {
    	this.amount = menge;
    }


    public Measure add(Measure g)
    {
        return new Measure(getAmount() + g.getAmount(), "cl");
    }

   
    public Measure substract(Measure g)
    {
        return new Measure(getAmount() - g.getAmount(), "cl");
    }

    public Measure multiply(double faktor)
    {
        return new Measure(getAmount() * faktor, "cl");
    }
}
