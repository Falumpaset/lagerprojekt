package backEnd.resources;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import dev.morphia.annotations.Entity;

@Entity(value = "password", noClassnameStored = true)
public class Password extends AbstractEntity implements Serializable {

	/**
	 * Serializable final Password for a User
	 */
	private static final long serialVersionUID = -3060437458916067L;

	public Password()
	{
		
	}
	
	@JsonIgnore
	private String password;

	@JsonCreator
	public Password(@JsonProperty("password") String password) {
		this.password = password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return this.password;
	}

	@Override
	public String toString() {
		
		return "Password [ ]";
	}
}
