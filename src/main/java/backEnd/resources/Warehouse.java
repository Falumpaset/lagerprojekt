/**
 * 
 */
package backEnd.resources;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import backEnd.database.MongoDriver;
import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Indexed;

/**
 * @author marcjager
 *
 */

@Entity(value = "lager", noClassnameStored = true)
public class Warehouse extends AbstractEntity {
	@Indexed
	private String name;

	@Embedded
	private HashMap<ObjectId, StockMemory> overallStock = new HashMap<>();
	@Embedded
	private Map<ObjectId, Integer> stockOnTheWay = new HashMap<>();

	private int minimumStockTime;
	private int timeTillRestock;

	private long startTime;

	public Warehouse() {
		super();
	}

	/**
	 * Creates a new Warehouse
	 * 
	 * @param name             Name
	 * @param minimumStockTime minute representation critical Mass until a new Order
	 *                         of a specific Resource is triggered
	 * @param timeTillRestock  minute representation of how much Resource is ordered
	 * 
	 */
	@JsonCreator
	public Warehouse(@JsonProperty("name") String name, @JsonProperty("minimumStockTime") int minimumStockTime,
			@JsonProperty("timeTillRestock") int timeTillRestock) {

		this.minimumStockTime = minimumStockTime;
		this.name = name;
		this.timeTillRestock = timeTillRestock;

		startTime = System.nanoTime();

	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setMinimunStockTime(int time) {
		this.minimumStockTime = time;
	}

	public int getMinimumStockTime() {
		return this.minimumStockTime;
	}

	public void setOverallStock(HashMap<ObjectId, StockMemory> overallStock) {
		this.overallStock = overallStock;
	}

	public void setTimeTillRestock(int newTime) {
		this.timeTillRestock = newTime;
	}

	public int getTimeTillRestock() {
		return this.timeTillRestock;
	}

	/**
	 * Stocks every Resource given
	 * 
	 * @param recources the Resources to Stock
	 */

	public void multiStock(WarehouseOrder resources) {
		resources.entrySet().forEach(x -> {
			soloStock(x.getKey(), x.getValue().intValue());

		});
		// TODO db load
	}

	public void soloStock(ObjectId recources, int amount) {
		if (overallStock.containsKey(recources)) {
			overallStock.get(recources).stock(amount);

		} else {
			overallStock.put(recources, new StockMemory(amount));

		}

		MongoDriver.updateStock(this.getClass(), this.id, recources, overallStock.get(recources));
	}

	public StockMemory getStockMemory(ObjectId resource) {
		return overallStock.get(resource);
	}

	/**
	 * Returns if the Warehouse can deliver the given Order
	 * 
	 * @param bestellung The Order
	 * @return True or False
	 */
	public <E extends Order> boolean canDeliverOrder(E order) {
		return order.keySet().stream().allMatch(x -> canDeliverStock(x, order.get(x).doubleValue()) == true);
	}

	/**
	 * Returns if this Warehouse can deliver the given Stock
	 * 
	 * @param resource Stock
	 * @param amount   Quantity
	 * @return boolean True or False
	 */
	public boolean canDeliverStock(ObjectId resource, double amount) {

		return overallStock.containsKey(resource) ? overallStock.get(resource).getActualStock() >= amount : false;
	}

	/**
	 * Returns actual Stock of given Resource
	 * 
	 * @param resource the Resource in question
	 * @return double Stock
	 */
	public double getStockFor(ObjectId resource) {

		return !overallStock.containsKey(resource) ? 0.0 : overallStock.get(resource).getActualStock();

	}

	/**
	 * Returns the Unit per Minute of the given Rohstoff
	 * 
	 * @param resource Rohstoff
	 * 
	 */

	// currentTime for test
	public double calculateTrend(ObjectId resource) throws NullPointerException {

		if (overallStock.containsKey(resource)) {
			StockMemory memory = overallStock.get(resource);
			long timePassed = (System.nanoTime() + TimeUnit.MINUTES.toNanos(10) - startTime);
			// long vergangeneZeit = (System.nanoTime() - startTime);
			long timePassedMinutes = TimeUnit.NANOSECONDS.toMinutes(timePassed);

			double trend = (memory.getDifference()

					/ timePassedMinutes);

			overallStock.get(resource).setTrend(trend);
			return trend;
		} else {
			return 0.0;
		}		

	}

	/**
	 * Updates all the Trends in Stock
	 * 
	 */
	private void updateAllTrends() {

		overallStock.keySet().forEach(x -> calculateTrend(x));
	}

	/**
	 * 
	 * Checks Stock Trend to determine which Stock needs to be ordered depending on
	 * set Triggertime includes Stock within Offset
	 * 
	 * @param Stock the Stock to be checked
	 * @return needed
	 */

	public boolean checkTrend(ObjectId stock) {

		if (overallStock.containsKey(stock)) {
			var sp = overallStock.get(stock);

			return sp.getActualStock() / sp.getTrend() < minimumStockTime * 1.05;
		} else
			return false;
	}

	/**
	 * Triggers Orders for needed Products in Stock based on actual Trend and given
	 * CutoffTime
	 * 
	 * @param stock
	 */
	private void checkNeededProducts() {

		updateAllTrends();
		HashSet<ObjectId> neededStock = new HashSet<ObjectId>();
		this.overallStock.keySet().stream().filter(x -> checkTrend(x)).forEach(x -> neededStock.add(x));
		createOrder(neededStock);
	
	}

	/**
	 * Unstocks Single Stock, checks trend and potentially triggers overall Stock
	 * check
	 * 
	 * @param stock    the Stock to unstock
	 * @param quantity the quantity to unstock
	 * @return
	 */
	public void unStock(ObjectId stock, double quantity) {

		this.overallStock.get(stock).unStock(quantity);
		calculateTrend(stock);

		if (checkTrend(stock)) {
			checkNeededProducts();
		}
		MongoDriver.updateStock(this.getClass(), this.id, stock, this.overallStock.get(stock));

	}

	/**
	 * Unstocks the given Order returns if sucessfull
	 * 
	 * @param order the Order to unstock
	 * @return sucess
	 */
	public <E extends Order> boolean unstockOrder(E order) {

		if (canDeliverOrder(order)) {
			for (ObjectId res : order.keySet()) {
				unStock(res, order.get(res).doubleValue());

			}

			return true;
		}
		return false;
	}

	/**
	 * Returns the actual Trend (Units/Minute)
	 * 
	 * @param resource stock
	 * @return Trend Units/Min
	 */
	public double getTrendFor(ObjectId resource) {
		var trend = overallStock.get(resource).getTrend();
		return trend;
	}

	public void createOrder(Set<ObjectId> rohstoffe) {

		WarehouseOrder neededResources = new WarehouseOrder();
		for (ObjectId stoff : rohstoffe) {

			int neededAmount = (int) Math.ceil(
					(timeTillRestock * overallStock.get(stoff).getTrend() - overallStock.get(stoff).getActualStock()));

			neededResources.put(stoff, neededAmount);
			stockOnTheWay.put(stoff, neededAmount);

		}

		// TODO rest benoetigeLieferung(benoetigteMengen);

	}

	public <E extends Order> void sendOrderToWarehouse(Warehouse recipient, E order) {
//TODO dokumentation
		unstockOrder(order);
	}

	/**
	 * Manually set Stock for a Resource, creates new StockMemory for it, old will
	 * be deleted if exisiting
	 * 
	 * @param resource
	 * @param stock
	 */
	public void setStockForResource(Resource resource, int stock) {
		var stockMemory = new StockMemory(stock);

		if (overallStock.containsKey(resource.getId())) {
			overallStock.replace(resource.getId(), stockMemory);
		} else {
			overallStock.put(resource.getId(), stockMemory);
		}

		MongoDriver.updateStock(this.getClass(), this.getId(), resource.getId(), stockMemory);
	}

	/**
	 * Resets the Trend of all stored Resources
	 */
	public void resetTrends() {
		overallStock.entrySet().forEach(e -> {
			e.getValue().setTrend(0.0);
		});
	}

	/**
	 * Returns overall Stock
	 * 
	 * @return The Overall Stock
	 */
	public Map<ObjectId, StockMemory> getStock() {
		return overallStock;
	}

	public Map<ObjectId, Integer> getStockOnTheWay() {
		System.out.println(this.stockOnTheWay);
		return this.stockOnTheWay;
	}

	@Override
	public String toString() {

		return "Lager [id=" + id + ",name=" + name + "]";
	}

}
