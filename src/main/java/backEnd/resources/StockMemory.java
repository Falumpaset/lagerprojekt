package backEnd.resources;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import dev.morphia.annotations.Embedded;

@Embedded
public class StockMemory
{

	private double initialStock;

	private double actualStock;

	private double difference;

	private double trend;

	
	public StockMemory() {
	
	}
	@JsonCreator
	public StockMemory(@JsonProperty("initialStock") double initialStock) {

		this.initialStock = initialStock;
		actualStock = initialStock;
		updateDifference();

	}

	public void stock(double amount) {
		initialStock += amount;
		actualStock += amount;
		updateDifference();

	}

	public double getInitialStock() {
		return this.initialStock;
	}

	public void setInitialStock(double amount) {
		this.initialStock = amount;
	}

	public void unStock(double amount) {

		actualStock -= amount;
		updateDifference();
	}

	public double getActualStock() {
		return actualStock;
	}

	public void setAcutalStock(double stock) {
		this.actualStock = stock;
	}

	public double getDifference() {
		return difference;
	}

	public void setDifference(double difference) {
		this.difference = difference;
	}

	private void updateDifference() {
		difference = initialStock - actualStock;
	}

	public double getTrend() {
		return trend;
	}

	public void setTrend(double trend) {
		this.trend = trend;
	}

	@Override
	public String toString() {

		return "BestandSpeicher [anfangsBestand=" + initialStock + ",aktuellerBestand="
				+ actualStock + ", differenz=" + difference + ", trend=" + trend + "]";
	}

}
