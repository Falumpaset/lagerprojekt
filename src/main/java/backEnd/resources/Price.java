package backEnd.resources;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import dev.morphia.annotations.Embedded;

@Embedded
public final class Price {

	private int euroPortion;
	private int centPortion;

	public Price() {

	}

	/**
	 * Wählt einen Geldbetrag aus.
	 * 
	 * @param eurocent Der Betrag in ganzen Euro-Cent
	 * 
	 * @require eurocent >= 0;
	 */

	public Price(int eurocent) {
		assert eurocent >= 0 : "Vorbedingung verletzt: eurocent >= 0";
		euroPortion = eurocent / 100;
		centPortion = eurocent % 100;
	}

	/**
	 * Wählt einen Geldbetrag aus.
	 * 
	 * @param eurocent Der Betrag in Euro,Cent
	 * 
	 * @require euro >= 0 && cent >= 0;
	 */

	@JsonCreator
	public Price(@JsonProperty("euro") int euro, @JsonProperty("cent") int cent) {
		assert euro >= 0 && cent >= 0 : "PreCondition violated: eurocent >= 0";
		euroPortion = euro;
		centPortion = cent;
	}

	public Price(String sbetrag) {
		sbetrag.replaceAll(",", ".");

		String euros = "";
		String cents = "";

		boolean atcents = false;

		// Wir gehen durch und fügen alles zu euros hinzu
		// Wenn wir ein . sehen dann atcents = true
		// dann wird alles darauffolgende den cents hinzugefügt

		// müll wird ignoriert
		for (int i = 0; i < sbetrag.length(); i++) {
			if (sbetrag.charAt(i) != '.' && !Character.isDigit(sbetrag.charAt(i))) {
				continue;
			}

			if (sbetrag.charAt(i) == '.') {
				atcents = true;
				continue;
			}

			if (!atcents) {
				euros += sbetrag.charAt(i);
				continue;
			}

			cents = cents + sbetrag.charAt(i);
		}

		euroPortion = Integer.parseInt(euros);
		centPortion = Integer.parseInt(cents);

	}

	/**
	 * Gibt den Eurobetrag ohne Cent zurück.
	 * 
	 * @return Den Eurobetrag ohne Cent.
	 */
	public int getEuroPortion() {
		return euroPortion;
	}

	/**
	 * Gibt den Centbetrag ohne Eurobetrag zurück.
	 */
	public int getCentPortion() {
		return centPortion;
	}

	/**
	 * Liefert einen formatierten String des Geldbetrags in der Form "10,23" zurück.
	 * 
	 * @return eine String-Repräsentation.
	 */
	@JsonIgnore
	public String getFormattedString() {
		return euroPortion + "." + getFormattedCentPortion();
	}

	/**
	 * Liefert einen zweistelligen Centbetrag zurück.
	 * 
	 * @return eine String-Repräsentation des Cent-Anteils.
	 */
	@JsonIgnore
	private String getFormattedCentPortion() {
		String result = "";
		if (centPortion < 10) {
			result += "0";
		}
		result += centPortion;
		return result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime + centPortion;
		result = prime * result + euroPortion;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof Price) {
			Price other = (Price) obj;
			result = (centPortion == other.centPortion) && (euroPortion == other.euroPortion);
		}
		return result;
	}

	/**
	 * Gibt diesen Geldbetrag in der Form "10,21" zurück.
	 */
	@JsonIgnore
	@Override
	public String toString() {
		return getFormattedString();
	}

	@JsonIgnore
	public int getEuroCent() {
		return this.centPortion + this.euroPortion * 100;
	}

	// Addieren von zwei Geldbeträgen
	public Price add(Price g) {
		return new Price(getEuroCent() + g.getEuroCent());
	}

	// Subtrahieren zweier Geldbeträge
	public Price subtract(Price g) {
		return new Price(getEuroCent() - g.getEuroCent());
	}

	// Vergleichen ob Betrag größer oder gleich a
	public boolean BiggerEq(Price a) {
		return getEuroCent() >= a.getEuroCent();
	}

	
	public Price multiply(double faktor) {
		assert faktor >= 0 : "Faktor is negative!";
		return new Price((int) (getEuroCent() * faktor));
	}

}
