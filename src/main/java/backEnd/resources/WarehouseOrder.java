package backEnd.resources;

import org.bson.types.ObjectId;

public class WarehouseOrder extends Order
{
    /**
     * HashMap to store Resourcetransfers between Warehouses
     */
    private static final long serialVersionUID = 1144236740505652820L;

    public String getFormattedString(int number, Warehouse recipient,
            Warehouse sender)
    {
        String output = "Bestellung: " + number + "." + "\n Lager: "
                + sender.getName() + "an Lager " + recipient.getName();
        for (ObjectId stoff : super.keySet())
        {
            output = output + "\n" + stoff + super.get(stoff);
        }
        return output;
    }
}
