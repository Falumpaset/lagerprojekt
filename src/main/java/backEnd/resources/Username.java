package backEnd.resources;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import dev.morphia.annotations.Entity;

@Entity(value = "username", noClassnameStored = true)
public class Username extends AbstractEntity implements Serializable {

	/**
	 * A Serializable Username for a User
	 */
	private static final long serialVersionUID = -277808764679284081L;
	private String username;

	public Username() {

	}

	@JsonCreator
	public Username(@JsonProperty("username") String username) {
		this.username = username;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "Username [name= " + username +"]";
	}

}
