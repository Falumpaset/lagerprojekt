package backEnd.resources;

import java.util.HashMap;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Indexed;

@Entity(value = "podukte", noClassnameStored = true)
public class Product extends AbstractEntity implements ProductService {
	@Indexed
	private String name;

	@Embedded
	private HashMap<ObjectId, Measure> ingredients = new HashMap<>();;

	private Price price;
	
	private double amount;

	private Category type;

	public enum Category {
		CocktailAlkoholFrei, Cocktail, Flasche, FlascheAlkoholfrei, Allgemein;
	}

	public Product() {

	}

	@JsonCreator
	public Product(@JsonProperty("name") String name, @JsonProperty("ingredients") HashMap<ObjectId, Measure> ingredients,
			@JsonProperty("price") Price price, @JsonProperty("category") Category category,
			@JsonProperty("amount") double amount) {
		this.name = name;

		this.ingredients = ingredients;

		this.price = price;

		this.type = category;

		this.amount = amount;

	}

	
	public void addIngredient(Resource ingredient, Measure amount) {
		if(containsIngredient(ingredient))
		{
		ingredients.put(ingredient.getId(), amount);
		}
	}

	
	public void deleteIngredient(Resource ingredient) {
		assert containsIngredient(ingredient);
		ingredients.remove(ingredient.getId());
	}

	private boolean containsIngredient(Resource product) {
		return ingredients.containsKey(product.getId());
	}

	
	public void setName(String neuerName) {
		name = neuerName;
	}

	
	public String getName() {
		return name;
	}

	
	public Category getCategory() {
		return type;
	}

	
	public void setCategory(Category kategorie) {
		this.type = kategorie;
	}


	
	public HashMap<ObjectId, Measure> getIngedients() {

		return ingredients;

	}
	
	public void setIngedients(HashMap<ObjectId, Measure> ingedients)
	{
		this.ingredients = ingedients;
	}

	@Override
	public double getAmount() {
		return amount;
	}

	@Override
	public Price getPrice() {
		return price;

	}

	public Price getPreisFuerMenge(int amount) {

		return getPrice().multiply(amount);
	}

	@Override
	public void setPrice(Price newPrice) {
		this.price = newPrice;
	}

	@JsonIgnore
	@Override
	public String getFormattedString() {
		return getName() + " " + getAmount() + "l" + " " + getPrice() + "€";
	}
	
	@JsonIgnore
	public String getFormattedStringAmount(int amount) {
		return amount + "x" + " " + getName() + " " + getAmount() + "l" + " " + getPrice() + "€" + " "
				+ getPrice().multiply(amount) + "€";
	}

	@Override
	public int hashCode() {
		return (int) (name.hashCode() + amount + price.hashCode() + ingredients.hashCode() + type.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof Product) {
			Product eins = (Product) obj;
			return getPrice().equals(eins.getPrice()) && getName().equals(eins.getName())
					&& getCategory().equals(eins.getCategory()) && getIngedients().equals(eins.getIngedients());

		}
		return result;
	}

	@Override
	public String toString() {
		return "Produkt [id=" + id + ",name=" + name + ",menge" + amount + "]";
	}

}
