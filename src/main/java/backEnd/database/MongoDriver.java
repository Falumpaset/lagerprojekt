package backEnd.database;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import backEnd.resources.AbstractEntity;
import backEnd.resources.StockMemory;
import dev.morphia.Datastore;
import dev.morphia.UpdateOptions;
import dev.morphia.query.Query;
import dev.morphia.query.UpdateOperations;

public class MongoDriver {

	private static Datastore mongoDB;

	public MongoDriver() {
		mongoDB = MongoDB.instance().getDatabase();
	}

	/**
	 * Saves the given Object to DB
	 * @param <E> the Object needs to extend AbstractEntity
	 * @param object The Object to save
	 * @return the Object
	 */
	public static <E extends AbstractEntity> E save(E object) {
		mongoDB.save(object);
		return object;
	}

	/**
	 * Saves List of Objects to DB
	 * @param <E> the Objects need to extend AbstractEntity
	 * @param objects List to save
	 * @return the saved Objects
	 */
	public static <E extends AbstractEntity> List<E> saveAll(List<E> objects) {
		var objectList = new ArrayList<E>();
		for (E element : objects) {
			objectList.add(save(element));
		}
		return objectList;
	}

	/**
	 * Returns DB Object with given ID
	 * @param <E> the Object needs to extend AbstractEntity
	 * @param category Collection to search in
	 * @param id the ID of the Object to retrieve
	 * @return the Object
	 */
	public static <E extends AbstractEntity> E getById(Class<E> category, ObjectId id) {

		return mongoDB.createQuery(category).disableValidation().field("_id").equal(id).first();

	}
	
	/**
	 * Returns first Object of Collection	
	 * @param <E> the Object extends AbstractEntity
	 * @param category Collection to search in
	 * @return the first Object in given Collection
	 */
	
	public static <E extends AbstractEntity> E getFirst(Class<E> category)
	{
		return mongoDB.createQuery(category).first();
	}

	/**
	 * Returns Element with matching Name
	 * @param <E> the Object needs to extend AbstractEntity
	 * @param category Category to look in
	 * @param name Name to look for
	 * @return the Object
	 * @throws IllegalArgumentException given Name matches no stored name
	 */
	public static <E extends AbstractEntity> E getByName(Class<E> category, String name) throws IllegalArgumentException {

		var object = mongoDB.createQuery(category).disableValidation().field("name").equal(name).first();
		if (object == null)
		{
			throw new IllegalArgumentException();
		}
		return object;

	}
	
	/**
	 * Returns List of Objects by their Ids. Need to be of same Category
	 * @param <E> the Objects need to extend AbstractEntity
	 * @param category Category to search in
	 * @param ids Ids to retrieve
	 * @return list of matching Objects
	 */
	public static <E extends AbstractEntity> List<E> getByIdList (Class <E> category, List<ObjectId> ids)
	{
		List<E> result = new ArrayList<E>();
		for (ObjectId id : ids)
		{
			result.add(getById(category, id));
		}
		
		return result;
	}

	/**
	 * Returns all Objects within a collection
	 * @param <E> the Objects need to extend AbstractEntity
	 * @param category Category to recieve
	 * @return all Objects within given Category
	 */ 
	public static <E extends AbstractEntity> List<E> getAll(Class<E> category) {
		return mongoDB.createQuery(category).find().toList();
	}

	/**
	 * Updates given Field of Entity
	 * @param <E> the Object need to extends AbstractEntity
	 * @param category Category to look in 
	 * @param id Id of Entity to modify
	 * @param field field to modify
	 * @param newString new Value
	 */
	public static <E extends AbstractEntity> void updateStringField(Class<E> category, ObjectId id, String field,
			String newString) {
		Query<E> q = mongoDB.createQuery(category).filter("_id", id);
		UpdateOperations<E> obs = mongoDB.createUpdateOperations(category).set(field, newString);

		mongoDB.update(q, obs);
	}

	/**
	 * Updates the StockMemory of given Entity
	 * @param <E> the Object needs to extend AbstractEntity
	 * @param object the Entity to modify
	 * @param warehouseId Id of Warehouse of Stock to modify
	 * @param recource Id of Resource to modify
	 * @param newStock new StockValue
	 */
	public static <E extends AbstractEntity> void updateStock(Class<E> object, ObjectId warehouseId, ObjectId recource,
			StockMemory newStock) {

		var options = new UpdateOptions();
		options.upsert(true);

		var update = mongoDB.createUpdateOperations(object).set("overallStock." + recource, newStock);

		mongoDB.update(mongoDB.createQuery(object).filter("_id", warehouseId), update, options);

	}

//TODO
	public static <E extends AbstractEntity> E getFeld(Class<E> objekt, ObjectId lagerId) {

		Query<E> q = mongoDB.createQuery(objekt).disableValidation().field("id").equal(lagerId);
		 return q.first();

	}
	
	//testmethod for setting embedded db 
	public static void setMongo(Datastore store)
	{
		mongoDB = store;
	}

}
