package backEnd.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ReadPreference;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;

import backEnd.resources.AbstractEntity;
import dev.morphia.Datastore;
import dev.morphia.Morphia;

public class MongoDB {

	public final static String DB_HOST = "localhost";

	public final static int DB_PORT = 27017;

	public final static String DB_NAME = "MorphiaDB";

	private static final MongoDB INSTANCE = new MongoDB();

	private Datastore dataStore;

	private MongoDB() {

		MongoClientOptions mongoOptions = MongoClientOptions.builder().socketTimeout(60000) 
				.connectTimeout(15000)
										
				.maxConnectionIdleTime(600000) 
												
				.readPreference(ReadPreference.primaryPreferred()) 
																	
				.writeConcern(WriteConcern.ACKNOWLEDGED).build();

		MongoClient mongoClient = new MongoClient(new ServerAddress(DB_HOST, DB_PORT), mongoOptions);

		dataStore = new Morphia().mapPackage(AbstractEntity.class.getPackageName()).createDatastore(mongoClient,
				DB_NAME);
		dataStore.ensureIndexes();
		dataStore.ensureCaps();
	

		

	}

	public static MongoDB instance() {
		return INSTANCE;
	}

	public Datastore getDatabase() {
		return dataStore;
	}

}
