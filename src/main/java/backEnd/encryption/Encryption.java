package backEnd.encryption;

import org.mindrot.jbcrypt.BCrypt;

public class Encryption {
	
	public static String encrypt(String password)
	{
		return BCrypt.hashpw(password, BCrypt.gensalt(12));
	}
	
	public static boolean decrypt (String password, String hashedPassword)
	{
		return BCrypt.checkpw(password, hashedPassword);
	}

}
